import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Loadout from "./components/Loadout";

Vue.use(Router);

//TODO: Refactor routes to include nested children
export default new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/loadout/:school/:character/",
      name: "Loadout",
      component: Loadout
    },
    {
      path: "/loadout/:school/:character/:loadoutId",
      component: Loadout
    }
  ]
});
