import firebase from "firebase";

var config = {
  apiKey: "AIzaSyBT9pL02ku_hUvxPSW5o4vqyf4ZphNKKOg",
  authDomain: "achbre-breach.firebaseapp.com",
  databaseURL: "https://achbre-breach.firebaseio.com",
  projectId: "achbre-breach",
  storageBucket: "achbre-breach.appspot.com",
  messagingSenderId: "977046294521"
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore();
