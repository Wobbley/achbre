import slugify from "slugify";

export default function slugIt(string) {
  return slugify(string, {
    replacement: "-",
    remove: /[*+~.()'"!:@]/g,
    lower: true
  });
}
